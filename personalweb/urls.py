from django.conf.urls import url
from .views import index
from .views import profil
from .views import kemampuan
from .views import kontak
from .views import form
urlpatterns = [
	url(r'^$', index, name ='index'),
	url(r'^profil/', profil, name ='profil'),
	url(r'^kemampuan/', kemampuan, name ='kemampuan'),
	url(r'^kontak/', kontak, name ='kontak'),
	url(r'^form/', form, name ='form'),
	
]